# 3. Як вхідні дані візьмемо ціле число; Це буде ціле число від 101 до 999,
# а його остання цифра не дорівнює нулю(робити перевірку не обовʼязково).
#
# Виведіть число, що складається з чисел першого у зворотньому порядку.
# Наприклад: 256 → 652.

def reverse_number(n):
  n_str = str(n)
  reversed_str = n_str[::-1]
  return int(reversed_str)


number = int(input())
reversed_number = reverse_number(number)
print(f"{number} → {reversed_number}")