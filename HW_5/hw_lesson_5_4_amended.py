# 4. Запитайте два цілих числа та виведіть:
#
# a. Їхню суму
# b. Їхня різниця
# c. результат множення
# d. Результат поділу першого на друге
# e. Залишок від поділу першого на друге
# f. True, якщо перше число більше або дорівнює другому, інакше False.

a = int(input("Напишіть перше ціле число: "))
b = int(input("Напишіть друге ціле число: "))

result_add = a + b  # a
print(f"Сума дорівнює: {result_add}")

result_sub = a - b  # b
print(f"Різниця дорівнює: {result_sub}")

result_mult = a * b # c
print(f"Результат множення дорівнює: {result_mult}")

result_div = round(a / b, 2)  # d
print(f"Результат поділу дорівнює: {result_div}")

result_floor_div = a % b  # e
print(f"Результат залишку від поділу дорівнює: {result_floor_div}")

result_bool = a >= b
print(f"Результат: {result_bool}")