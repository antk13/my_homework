import requests

API_KEY = "e90fab7014064d2c88795d9fd95afa6f"


class OpenWeatherMap:
    def __init__(self, city=None, lat=None, lon=None):
        self.api_key = API_KEY
        self.base_url = "http://api.openweathermap.org/data/2.5/weather"
        self.city = city
        self.lat = lat
        self.lon = lon
        self.data = self._get_weather_data()

    def _get_weather_data(self):
        params = {
            'appid': self.api_key,
            'units': 'metric'
        }
        if self.city:
            params['q'] = self.city
        elif self.lat and self.lon:
            params['lat'] = self.lat
            params['lon'] = self.lon
        else:
            raise ValueError("Either city name or latitude and longitude must be provided.")

        response = requests.get(self.base_url, params=params)
        response.raise_for_status()
        return response.json()

    def get_temp(self):
        return self.data['main']['temp']

    def get_weather(self):
        return self.data['weather'][0]['description']

    def get_wind(self):
        return self.data['wind']['speed']

    def get_city(self):
        return self.data['name']

    def get_text(self):
        return (f"Weather in {self.get_city()}:\n"
                f"Temperature: {self.get_temp()}°C\n"
                f"Weather: {self.get_weather()}\n"
                f"Wind speed: {self.get_wind()} m/s")

    def get_any_key(self, *keys):
        results = {key: self.data.get(key, 'Not Found') for key in keys}
        return results

    def __str__(self):
        return self.get_text()


def main():
    city = input("Enter city name: ")
    weather = OpenWeatherMap(city=city)
    print(weather)


if __name__ == "__main__":
    main()
