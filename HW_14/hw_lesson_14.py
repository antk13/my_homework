import re

with open('orders.txt', 'r') as file:
    data = file.read()

products = re.split(r'@@@|\s{2,}', data)

product_count = {}

for product in products:
    product = product.strip()
    if product in product_count:
        product_count[product] += 1
    else:
        product_count[product] = 1

sorted_product_count = dict(sorted(product_count.items(), key=lambda item: (-item[1], item[0])))


for product, count in sorted_product_count.items():
    print(f"{product}: {count}")

