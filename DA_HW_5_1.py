# 1. Напишіть програму на Python
# а) щоб завантажити дані ірису з вказаного файлу csv у dataframe та надрукувати форму даних, тип даних та перші 3 рядки.
import pandas as pd
def load_iris_data_from_csv(file_path):
    try:
        df = pd.read_csv(file_path)
        print("Форма даних (рядки, стовпці):", df.shape)
        print("Тип даних у кожному стовпці:\n", df.dtypes)
        print("Перші 3 рядки даних:\n", df.head(3))
        return df

    except FileNotFoundError:
        print(f"Помилка: Файл '{file_path}' не знайдено.")
    except pd.errors.EmptyDataError:
        print("Помилка: Файл порожній.")
    except pd.errors.ParserError:
        print("Помилка: Неправильний формат CSV файлу.")
    except Exception as e:
        print(f"Виникла помилка: {e}")

file_path = r'C:\Users\HP\PycharmProjects\dan_it_lessons\homework\Iris.csv'

df = load_iris_data_from_csv(file_path)

# b) за допомогою Scikit-learn, щоб надрукувати ключі, кількість рядків-стовпців, назви ознак та опис даних Ірису.
from sklearn import datasets
def load_iris_data_sklearn():
    iris = datasets.load_iris()
    print("Ключі даних:", iris.keys())
    print("Форма даних (рядки, стовпці):", iris.data.shape)
    print("Назви ознак:", iris.feature_names)
    print("Опис даних:\n", iris.DESCR)

load_iris_data_sklearn()

# c) щоб переглянути базові статистичні деталі, як-от перцентиль, середнє, стандартне відхилення тощо даних ірису.
import pandas as pd
from sklearn import datasets

def load_iris_data_sklearn():
    iris = datasets.load_iris()
    df = pd.DataFrame(data=iris.data, columns=iris.feature_names)
    df['species'] = iris.target  # Додаємо стовпець з мітками видів
    return df

def basic_statistics(df):
    print("Базова статистика:\n", df.describe())

df = load_iris_data_sklearn()  # Завантажуємо дані у DataFrame
basic_statistics(df)  # Переглядаємо базову статистику

# d) щоб отримати спостереження кожного виду (сетоза, версиколор, віргініка) з даних ірису.
import pandas as pd
from sklearn import datasets

def load_iris_data_sklearn():
    iris = datasets.load_iris()
    df = pd.DataFrame(data=iris.data, columns=iris.feature_names)
    df['species'] = iris.target
    return df

def observations_per_species(df):
    species_counts = df['species'].value_counts()
    print("Спостереження для кожного виду:\n", species_counts)

df = load_iris_data_sklearn()
observations_per_species(df)

# e) щоб створити графік для отримання загальної статистики даних Ірис.
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
from sklearn import datasets

def load_iris_data_sklearn():
    iris = datasets.load_iris()
    df = pd.DataFrame(data=iris.data, columns=iris.feature_names)
    df['species'] = iris.target  # Додаємо стовпець з мітками видів
    return df

def visualize_general_statistics(df):
    sns.pairplot(df, hue='species', palette='husl')
    plt.title('Парний графік для даних Ірису')
    plt.show()

df = load_iris_data_sklearn()
visualize_general_statistics(df)

# f) Напишіть програму на Python, щоб створити стовпчасту діаграму для визначення частоти трьох видів Ірис.
import pandas as pd
import matplotlib.pyplot as plt
from sklearn import datasets

def load_iris_data_sklearn():
    iris = datasets.load_iris()
    df = pd.DataFrame(data=iris.data, columns=iris.feature_names)
    df['species'] = iris.target
    return df

def species_bar_chart(df):
    species_counts = df['species'].value_counts()
    species_counts.plot(kind='bar', color=['blue', 'green', 'red'])
    plt.title('Частота трьох видів Ірису')
    plt.xlabel('Вид')
    plt.ylabel('Частота')
    plt.xticks(ticks=range(len(species_counts)), labels=['Iris-setosa', 'Iris-versicolor', 'Iris-virginica'],
               rotation=0)
    plt.show()

df = load_iris_data_sklearn()
species_bar_chart(df)

# g) для розподілу набору даних ірисів на його атрибути (X) та мітки (y). Змінна X містить перші чотири стовпці (тобто атрибути), а y містить мітки набору даних. Натисніть тут, щоб побачити приклад розв'язку.
import pandas as pd
from sklearn import datasets

def load_iris_data_sklearn():
    iris = datasets.load_iris()
    df = pd.DataFrame(data=iris.data, columns=iris.feature_names)
    df['species'] = iris.target
    return df

def split_attributes_labels(df):
    X = df.iloc[:, :-1].values
    y = df.iloc[:, -1].values
    return X, y

df = load_iris_data_sklearn()
X, y = split_attributes_labels(df)

print("Атрибути (X):\n", X[:5])
print("Мітки (y):\n", y[:5])

# h) за допомогою Scikit-learn для розділення набору даних ірисів на 70% тренувальних даних та 30% тестових даних. З загальної кількості 150 записів, набір для тренування міститиме 120 записів, а тестовий набір - 30 з цих записів. Виведіть обидва набори даних.
import pandas as pd
from sklearn import datasets
from sklearn.model_selection import train_test_split

def load_iris_data_sklearn():
    iris = datasets.load_iris()
    df = pd.DataFrame(data=iris.data, columns=iris.feature_names)
    df['species'] = iris.target
    return df

def split_train_test(df, test_size=0.3, random_state=42):
    X = df.iloc[:, :-1].values
    y = df.iloc[:, -1].values
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=test_size, random_state=random_state)
    return X_train, X_test, y_train, y_test

df = load_iris_data_sklearn()
X_train, X_test, y_train, y_test = split_train_test(df)


print("Тренувальні дані (X_train):\n", X_train)
print("\nТестові дані (X_test):\n", X_test)
print("\nТренувальні мітки (y_train):\n", y_train)
print("\nТестові мітки (y_test):\n", y_test)

# i) Напишіть програму на Python за допомогою Scikit-learn для перетворення стовпців видів у числовий стовпець набору даних ірисів. Для кодування цих даних кожне значення перетворіть на число. Наприклад, Iris-setosa:0, Iris-versicolor:1 та Iris-virginica:2. Тепер виведіть набір даних ірисів на 80% тренувальних даних і 20% тестових даних. З загальної кількості 150 записів, набір для тренування міститиме 120 записів, а тестовий набір - 30 з цих записів. Виведіть обидва набори даних.
import pandas as pd
from sklearn import datasets
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import LabelEncoder

def load_iris_data_sklearn():
    iris = datasets.load_iris()
    df = pd.DataFrame(data=iris.data, columns=iris.feature_names)
    df['species'] = iris.target
    return df

def encode_species(df):
    le = LabelEncoder()
    df['species'] = le.fit_transform(df['species'])
    return df

def split_train_test(df, test_size=0.2, random_state=42):
    X = df.iloc[:, :-1].values
    y = df.iloc[:, -1].values
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=test_size, random_state=random_state)
    return X_train, X_test, y_train, y_test

df = load_iris_data_sklearn()
df_encoded = encode_species(df)
X_train, X_test, y_train, y_test = split_train_test(df_encoded)


print("Тренувальні дані (X_train):\n", X_train)
print("\nТестові дані (X_test):\n", X_test)
print("\nТренувальні мітки (y_train):\n", y_train)
print("\nТестові мітки (y_test):\n", y_test)
print("\nДані з кодуванням видів:\n", df_encoded.head())

# j) Напишіть програму на Python за допомогою Scikit-learn для розділення набору даних ірисів на 70% тренувальних даних та 30% тестових даних. З загальної кількості 150 записів, набір для тренування міститиме 105 записів, а тестовий набір - 45 з цих записів. Прогнозуйте відповідь для тестового набору даних (SepalLengthCm, SepalWidthCm, PetalLengthCm, PetalWidthCm) за допомогою алгоритму найближчих сусідів (K Nearest Neighbor Algorithm). Використовуйте 5 як кількість сусідів.
import pandas as pd
from sklearn import datasets
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import LabelEncoder
from sklearn.neighbors import KNeighborsClassifier

def load_iris_data_sklearn():
    iris = datasets.load_iris()
    df = pd.DataFrame(data=iris.data, columns=iris.feature_names)
    df['species'] = iris.target
    return df

def encode_species(df):
    le = LabelEncoder()
    df['species'] = le.fit_transform(df['species'])
    return df

def split_train_test(df, test_size=0.3, random_state=42):
    X = df.iloc[:, :-1].values
    y = df.iloc[:, -1].values
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=test_size, random_state=random_state)
    return X_train, X_test, y_train, y_test

def knn_prediction(X_train, X_test, y_train, n_neighbors=5):
    knn = KNeighborsClassifier(n_neighbors=n_neighbors)
    knn.fit(X_train, y_train)
    y_pred = knn.predict(X_test)
    return y_pred


df = load_iris_data_sklearn()
df_encoded = encode_species(df)
X_train, X_test, y_train, y_test = split_train_test(df_encoded)

y_pred = knn_prediction(X_train, X_test, y_train)


print("Прогнозовані значення для тестового набору:\n", y_pred)
print("\nФактичні значення для тестового набору:\n", y_test)
