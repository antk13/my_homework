# У класі професора Грубла щойно був іспит. Він почав перевіряти роботи, але оцінював їх не дуже уважно.
#
# Напишіть програму, яка приймає як вхідні дані оцінку кожного учня і те, чи здав він іспит.
#
# Потім програмі необхідно надрукувати дві речі:
#
# a. Чи був професор Грубл послідовним у проставленні позначки "Passed" для студентів.

from pprint import pprint

data_1 = (("student_1", 78, "Failed"),
          ("student_2", 82, "Passed"),
          ("student_3", 97, "Passed"),
          ("student_4", 86, "Passed"),
          ("student_5", 67, "Passed"),
          ("student_6", 75, "Passed"))

data_1 = sorted(data_1, key=lambda student: student[1])
pprint(data_1)

max_failed_grade = None
min_passed_grade = None

for student_data in data_1:
    student = student_data[0]
    grade = student_data[1]
    result = student_data[2]

    if result == "Failed":
        if max_failed_grade is None or grade > max_failed_grade:
            max_failed_grade = grade
            failed_student = student
            failed_result = result

    if result == "Passed":
        if min_passed_grade is None or grade < min_passed_grade:
            min_passed_grade = grade
            passed_student = student
            passed_result = result

if max_failed_grade is not None and min_passed_grade is not None:
    if max_failed_grade >= min_passed_grade:
        print(f"Професор непослідовний. Оцінка {max_failed_grade} у студента {failed_student} з результатом {failed_result} більша, \nніж оцінка {min_passed_grade} у студента {passed_student} з результатом {passed_result}.")
    else:
        print("Професор послідовний.")




