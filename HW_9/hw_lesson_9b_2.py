# b. Якщо професор Грубл був послідовним, виведіть діапазон у якому знаходиться поріг для складання іспиту.

data_2 = (("student_1", 84, "Passed"),
          ("student_2", 78, "Passed"),
          ("student_3", 65, "Failed"),
          ("student_4", 90, "Passed"),
          ("student_5", 72, "Failed"))

max_failed_grade = None
min_passed_grade = None

for student_data in data_2:
    student = student_data[0]
    grade = student_data[1]
    result = student_data[2]

    if result == "Failed":
        if max_failed_grade is None or grade > max_failed_grade:
            max_failed_grade = grade

    if result == "Passed":
        if min_passed_grade is None or grade < min_passed_grade:
            min_passed_grade = grade

print(f"Поріг для складання іспиту: {max_failed_grade + 1}-{min_passed_grade}")

