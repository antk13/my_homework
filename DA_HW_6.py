# 2. Збір даних:
# Використовуйте набір даних penguins із бібліотеки Seaborn. Ці дані можна завантажити за допомогою:
import seaborn as sns
import pandas as pd

penguins = sns.load_dataset('penguins')
print(penguins.head())

# 3. Попередня обробка даних:
# Видалення записів з відсутніми значеннями.
# Кодування категоріальних змінних (вид, острів, стать) за допомогою One-Hot Encoding.

penguins_cleaned = penguins.dropna()
print(penguins_cleaned.isnull().sum())

penguins_encoded = pd.get_dummies(penguins_cleaned, columns=['species', 'island', 'sex'])
print(penguins_encoded.head())

# 4. Розподіл даних:
# Розділіть дані на тренувальний та тестовий набори (наприклад, в співвідношенні 80/20).

from sklearn.model_selection import train_test_split

penguins = sns.load_dataset('penguins')
penguins_cleaned = penguins.dropna()
penguins_encoded = pd.get_dummies(penguins_cleaned, columns=['species', 'island', 'sex'])

X = penguins_encoded.drop(columns=['species_Adelie', 'species_Chinstrap', 'species_Gentoo'])
y = penguins_encoded[['species_Adelie', 'species_Chinstrap', 'species_Gentoo']]

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)

print("Розмір тренувальних даних (X_train):", X_train.shape)
print("Розмір тестових даних (X_test):", X_test.shape)
print("Розмір тренувальних міток (y_train):", y_train.shape)
print("Розмір тестових міток (y_test):", y_test.shape)

# 5. Створення pipeline:
# Створіть pipeline, що включає кроки стандартизації числових змінних та використання класифікатора. Наприклад:
#
# StandardScaler() для масштабування числових даних.
# RandomForestClassifier() як модель для класифікації.

from sklearn.preprocessing import StandardScaler
from sklearn.ensemble import RandomForestClassifier
from sklearn.pipeline import Pipeline
from sklearn.metrics import classification_report

X = penguins_encoded.drop('species_Adelie', axis=1)
y = penguins_encoded['species_Adelie']

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)

pipeline = Pipeline([
    ('scaler', StandardScaler()),
    ('classifier', RandomForestClassifier(random_state=42))
])

pipeline.fit(X_train, y_train)

y_pred = pipeline.predict(X_test)

print(classification_report(y_test, y_pred))

# 6. Навчання та валідація моделі:
# Використовуйте cross_val_score для оцінки моделі на тренувальних даних. Перевірте модель на тестових даних.

from sklearn.model_selection import cross_val_score

cv_scores = cross_val_score(pipeline, X_train, y_train, cv=5)

print(f"Крос-валідаційні оцінки: {cv_scores}")
print(f"Середня крос-валідаційна оцінка: {cv_scores.mean()}")

pipeline.fit(X_train, y_train)

test_accuracy = pipeline.score(X_test, y_test)
print(f"Точність на тестових даних: {test_accuracy}")

# 7. Тонка настройка:
# Використовуйте GridSearchCV для пошуку найкращих параметрів для RandomForestClassifier у вашому pipeline.

from sklearn.model_selection import train_test_split, GridSearchCV
from sklearn.ensemble import RandomForestClassifier

X = penguins_encoded.drop('species_Adelie', axis=1)
y = penguins_encoded['species_Adelie']
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)

pipeline = Pipeline([
    ('scaler', StandardScaler()),
    ('classifier', RandomForestClassifier(random_state=42))
])


param_grid = {
    'classifier__n_estimators': [50, 100, 150],
    'classifier__max_depth': [None, 10, 20, 30],
    'classifier__min_samples_split': [2, 5, 10],
}

grid_search = GridSearchCV(pipeline, param_grid, cv=5, n_jobs=-1, scoring='accuracy')
grid_search.fit(X_train, y_train)

print("Найкращі параметри:", grid_search.best_params_)

best_model = grid_search.best_estimator_
test_accuracy = best_model.score(X_test, y_test)
print(f"Точність на тестових даних: {test_accuracy}")

# 8. Аналіз результатів:
# Оцініть точність, recall, F1-score та інші метрики для вашої кінцевої моделі.
# Використовуйте матрицю помилок для візуального представлення результатів.

from sklearn.metrics import classification_report, confusion_matrix
import matplotlib.pyplot as plt

y_pred = best_model.predict(X_test)

print("Звіт про класифікацію:\n", classification_report(y_test, y_pred))

conf_matrix = confusion_matrix(y_test, y_pred)

plt.figure(figsize=(8, 6))
sns.heatmap(conf_matrix, annot=True, fmt='d', cmap='Blues', xticklabels=['Non-Adelie', 'Adelie'], yticklabels=['Non-Adelie', 'Adelie'])
plt.title('Матриця помилок')
plt.xlabel('Прогнозований клас')
plt.ylabel('Фактичний клас')
plt.show()

# 9. Документація та висновки:
# Задокументуйте кроки вашого аналізу, параметри моделі та інтерпретацію результатів.
# Обговоріть можливі шляхи для покращення моделі.
#
# 1. Збір даних.
# Використання набору даних пінгвінів з бібліотеки Seaborn.
#
# 2. Попередня обробка даних.
# Видалення записів з відсутніми значеннями.
# Кодування категоріальних змінних (species, island, sex) за допомогою One-Hot Encoding.
#
# 3. Розподіл даних.
# Розділення даних на тренувальний (80%) та тестовий (20%) набори.
#
# 4.Створення pipeline.
# Стандартизація числових змінних за допомогою StandardScaler і використання RandomForestClassifier як моделі для класифікації.
#
# 5. Навчання та валідація моделі.
# Використання cross_val_score для оцінки моделі на тренувальних даних.
#
# 6. Тонка настройка.
# Використання GridSearchCV для пошуку найкращих параметрів для RandomForestClassifier у pipeline.
#
# 7. Аналіз результатів.
# Оцінка precision, recall, F1-score та інші метрики для кінцевої моделі.
# Візуалізація результатів за допомогою матриці помилок.

