# 1.Аналіз розподілу ваги та висоти птахів:
# Спробуйте побудувати графік, який відображає розподіл ваги та висоти птахів (пінгвінів) за допомогою Seaborn.
# Використовуйте відповідні графічні методи для визначення розподілу та залежності між цими двома ознаками.

import seaborn as sns
import matplotlib.pyplot as plt

penguins = sns.load_dataset("penguins")

print(penguins.head())

plt.figure(figsize=(12, 6))

plt.subplot(1, 2, 1)
sns.histplot(penguins['body_mass_g'].dropna(), kde=True, bins=30)
plt.title('Розподіл ваги пінгвінів')
plt.xlabel('Вага (г)')
plt.ylabel('Частота')

plt.subplot(1, 2, 2)
sns.histplot(penguins['flipper_length_mm'].dropna(), kde=True, bins=30)
plt.title('Розподіл висоти пінгвінів')
plt.xlabel('Висота (мм)')
plt.ylabel('Частота')

plt.figure(figsize=(8, 6))
sns.scatterplot(data=penguins, x='flipper_length_mm', y='body_mass_g', hue='species', style='species')
plt.title('Залежність між вагою та висотою пінгвінів')
plt.xlabel('Висота (мм)')
plt.ylabel('Вага (г)')
plt.legend(title='Вид')

plt.tight_layout()
plt.show()

# 2. Вивчення впливу виду птаха на розміри крил:
# Зобразіть графік, який показує розміри крил у залежності від виду птаха.
# Використовуйте різні кольори або стилі для різних видів птахів.
# Це дозволяє вам визначити, чи існують різниці у розмірах крил між різними видами пінгвінів.

plt.figure(figsize=(12, 6))

plt.subplot(1, 2, 1)
sns.scatterplot(data=penguins, x='species', y='flipper_length_mm', hue='species', style='species', s=100)
plt.title('Розміри крил у залежності від виду птаха (графік розсіяння)')
plt.xlabel('Вид птаха')
plt.ylabel('Розміри крил (мм)')

plt.subplot(1, 2, 2)
sns.boxplot(data=penguins, x='species', y='flipper_length_mm')
plt.title('Розміри крил у залежності від виду птаха (Boxplot)')
plt.xlabel('Вид птаха')
plt.ylabel('Розміри крил (мм)')

plt.tight_layout()
plt.show()

# 3. Кореляція між різними ознаками:
# Спробуйте побудувати теплову карту кореляції для всього набору даних пінгвінів.
# Вивчення кореляцій може допомогти зрозуміти, які ознаки співвідносяться між собою. Зробіть аналіз кореляцій та виберіть ті ознаки, які мають високий ступінь взаємозв'язку.

penguins_cleaned = penguins.dropna()

numerical_cols = penguins_cleaned.select_dtypes(include='number')

correlation_matrix = numerical_cols.corr()

plt.figure(figsize=(10, 8))
sns.heatmap(correlation_matrix, annot=True, fmt=".2f", cmap='coolwarm', square=True, cbar_kws={"shrink": .8})
plt.title('Теплова карта кореляції між ознаками пінгвінів')
plt.show()

high_corr = correlation_matrix[abs(correlation_matrix) > 0.5]
print(high_corr)
