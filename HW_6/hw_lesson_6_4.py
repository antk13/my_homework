# 4. Зіграйте у гру Fizz-Buzz: виведіть усі числа від 1 до 100;
# якщо число ділиться на 3, замість числа виведіть "fizz".
# Якщо воно ділиться на 5, замість числа виведіть "Buzz". Якщо воно ділиться на обидва, виведіть "fizz buzz" замість числа.


for number in range(1, 101):
    if number % 3 == 0 and number % 5 == 0:
        print("fizz buzz")
    elif number % 3 == 0:
        print("fizz")
    elif number % 5 == 0:
        print("Buzz")
    else:
        print(number)
