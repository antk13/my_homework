# 3. Як вхідні дані запитайте три числа і виведіть найменше, середнє та найбільше. Припустимо, всі вони різні

a = int(input("Введіть ціле число: "))
b = int(input("Введіть інше число: "))
c = int(input("Введіть інше число: "))


smallest_number = min(a, b, c)
print(f"Найменше число: {smallest_number}")

numbers = (a, b, c)
total = sum(numbers)
average = round(total / len(numbers))
print(f"Середнє: {average}")

maximum_number = max(a, b, c)
print(f"Найбільше число: {maximum_number}")

