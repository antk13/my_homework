# 6. Зіграйте у гру 7-boom: виведіть усі числа від 1 до 100; якщо число ділиться на 7 або містить цифру 7,
# виведіть "BOOM" замість числа.

for number in range(1, 101):
    if number % 7 == 0 or "7" in str(number):
        print("BOOM")
    else:
        print(number)
