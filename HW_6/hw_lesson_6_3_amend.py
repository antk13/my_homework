# 3. Як вхідні дані запитайте три числа і виведіть найменше, середнє та найбільше. Припустимо, всі вони різні

a = int(input("Введіть ціле число: "))
b = int(input("Введіть інше число: "))
c = int(input("Введіть інше число: "))


numbers = [a, b, c]
numbers.sort()
min_number = numbers[0]
median = numbers[1]
max_number = numbers[2]

print(f"Найменше: {min_number}")
print(f"Середнє: {median}")
print(f"Найбільше: {max_number}")
