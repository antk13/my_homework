# 2. Збір даних
import seaborn as sns

titanic = sns.load_dataset('titanic')
print(titanic)

# 3. Очищення та підготовка даних
# Обробка відсутніх значень: наприклад, заповніть відсутні віки середнім віком або прогнозуйте вік за допомогою іншої моделі.

titanic['age'] = titanic['age'].fillna(titanic['age'].mean())
titanic['embarked'] = titanic['embarked'].fillna(titanic['embarked'].mode()[0])

titanic_columns = ['survived', 'pclass', 'sex', 'age', 'sibsp', 'parch', 'fare', 'embarked']
titanic_subset = titanic[titanic_columns]

print("\nВибрані колонки:")
print(titanic_subset.head())

# Перетворення категорійних змінних на числові, наприклад, використання одноразового кодування для стовпців Embarked та Sex.
import pandas as pd

titanic_encoded = pd.get_dummies(titanic, columns=['sex', 'embarked'], drop_first=True)
print(titanic_encoded.head())


# Видалення характеристик, які можуть не вносити вкладу в модель, як-от Квиток та Ім'я (окрім випадків вилучення титулів з імен).

titanic_cleaned = titanic_encoded.drop(columns=['ticket', 'name'], errors='ignore')
print(titanic_cleaned.head())

# 4. Розвідувальний аналіз даних (EDA)
# Аналіз розподілу ключових характеристик та їх зв'язку з цільовою змінною, Вижив.
# Використання візуалізацій для кращого розуміння даних: гістограми, коробкові діаграми та точкові діаграми.
# Дослідження кореляцій між характеристиками.

import matplotlib.pyplot as plt
import seaborn as sns

# Візуалізація розподілу виживання
plt.figure(figsize=(8, 6))
sns.countplot(data=titanic_cleaned, x='survived')
plt.title('Розподіл виживання на Титаніку')
plt.xlabel('Вижив (0 = Ні, 1 = Так)')
plt.ylabel('Кількість пасажирів')
plt.xticks([0, 1], ['Ні', 'Так'])
plt.show()

# Візуалізація розподілу віку
plt.figure(figsize=(10, 6))
sns.histplot(data=titanic_cleaned, x='age', hue='survived', bins=30, kde=True, stat='density', common_norm=False)
plt.title('Розподіл віку пасажирів за виживанням')
plt.xlabel('Вік')
plt.ylabel('Щільність')
plt.legend(title='Вижив', labels=['Ні', 'Так'])
plt.show()

# Візуалізація аналізу класу пасажирів
plt.figure(figsize=(8, 6))
sns.countplot(data=titanic_cleaned, x='pclass', hue='survived')
plt.title('Виживання за класом пасажира')
plt.xlabel('Клас')
plt.ylabel('Кількість пасажирів')
plt.xticks([0, 1, 2], ['1-й', '2-й', '3-й'])
plt.legend(title='Вижив', labels=['Ні', 'Так'])
plt.show()

# Візуалізація аналізу тарифу
plt.figure(figsize=(10, 6))
sns.boxplot(data=titanic_cleaned, x='survived', y='fare')
plt.title('Тариф за виживання')
plt.xlabel('Вижив (0 = Ні, 1 = Так)')
plt.ylabel('Тариф')
plt.xticks([0, 1], ['Ні', 'Так'])
plt.show()

# Кореляційний аналіз
import numpy as np

titanic_cleaned = titanic_cleaned.select_dtypes(include=[np.number])

plt.figure(figsize=(10, 6))
correlation_matrix = titanic_cleaned.corr()
sns.heatmap(correlation_matrix, annot=True, cmap='coolwarm', fmt=".2f")
plt.title('Кореляційна матриця')
plt.show()
print(correlation_matrix)

# 5. Інженерія ознак
# Створення нових ознак, які можуть допомогти покращити модель, наприклад:
# FamilySize = SibSp + Parch + 1
titanic['FamilySize'] = titanic['sibsp'] + titanic['parch'] + 1

# IsAlone, бінарний індикатор того, чи є пасажир на самоті.
titanic['IsAlone'] = (titanic['FamilySize'] == 1).astype(int)

# Масштабування ознак, якщо використовуються моделі, чутливі до масштабування ознак, такі як SVM або KNN.
# SVM
from sklearn.preprocessing import StandardScaler

features_to_scale = ['age', 'fare', 'FamilySize']

scaler = StandardScaler()

titanic[features_to_scale] = scaler.fit_transform(titanic[features_to_scale])

print('SVM', titanic[features_to_scale].head())

# KNN
from sklearn.preprocessing import MinMaxScaler

scaler = MinMaxScaler()

titanic[features_to_scale] = scaler.fit_transform(titanic[features_to_scale])

print('KNN', titanic[features_to_scale].head())

# 6. Вибір моделі
# Розділіть дані на навчальний і тестовий набори для оцінки продуктивності ваших моделей.

from sklearn.model_selection import train_test_split

titanic_encoded = pd.get_dummies(titanic, columns=['sex', 'embarked', 'pclass'], drop_first=True)

X = titanic_encoded.drop('survived', axis=1)
y = titanic_encoded['survived']

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)

print('Розмір навчального набору:', X_train.shape)
print('Розмір тестового набору:', X_test.shape)

# Навчити різні моделі, щоб побачити, яка працює найкраще. Почніть з простих моделей, таких як логістична регресія, і переходьте до більш складних, як-от випадкові ліси, підсилювальне
# градієнтне навчання або навіть нейронні мережі.
print(X.dtypes)

numerical_cols = titanic_encoded.select_dtypes(include=['int64', 'float64']).columns

X = titanic_encoded[numerical_cols]

y = titanic_encoded['survived']

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)

scaler = StandardScaler()
X_train_scaled = scaler.fit_transform(X_train)
X_test_scaled = scaler.transform(X_test)

# Логістична регресія
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import accuracy_score

logistic_model = LogisticRegression()
logistic_model.fit(X_train_scaled, y_train)

y_pred_logistic = logistic_model.predict(X_test_scaled)
print(f"Логістична регресія Точність: {accuracy_score(y_test, y_pred_logistic):.4f}")

# Випадковий ліс
from sklearn.ensemble import RandomForestClassifier

rf_model = RandomForestClassifier(n_estimators=100, random_state=42)
rf_model.fit(X_train_scaled, y_train)

y_pred_rf = rf_model.predict(X_test_scaled)
print(f"Випадковий ліс Точність: {accuracy_score(y_test, y_pred_rf):.4f}")

# Градієнтне підсилення
from sklearn.ensemble import GradientBoostingClassifier

gb_model = GradientBoostingClassifier(random_state=42)
gb_model.fit(X_train_scaled, y_train)

y_pred_gb = gb_model.predict(X_test_scaled)
print(f"Градієнтне підсилення Точність: {accuracy_score(y_test, y_pred_gb):.4f}")

# Нейронна мережа
from sklearn.neural_network import MLPClassifier

mlp_model = MLPClassifier(hidden_layer_sizes=(100,), max_iter=500, random_state=42)
mlp_model.fit(X_train_scaled, y_train)

y_pred_mlp = mlp_model.predict(X_test_scaled)
print(f"Нейронна мережа Точність: {accuracy_score(y_test, y_pred_mlp):.4f}")

# Використовуйте перехресну перевірку для оцінки ефективності кожної моделі.
from sklearn.model_selection import cross_val_score

# Логістична регресія з перехресною перевіркою
logistic_model = LogisticRegression()
logistic_cv_scores = cross_val_score(logistic_model, X_train_scaled, y_train, cv=5, scoring='accuracy')
print(f"Логістична регресія: Середня точність = {logistic_cv_scores.mean():.4f}, Стандартне відхилення = {logistic_cv_scores.std():.4f}")

# Випадковий ліс з перехресною перевіркою
rf_model = RandomForestClassifier(n_estimators=100, random_state=42)
rf_cv_scores = cross_val_score(rf_model, X_train_scaled, y_train, cv=5, scoring='accuracy')
print(f"Випадковий ліс: Середня точність = {rf_cv_scores.mean():.4f}, Стандартне відхилення = {rf_cv_scores.std():.4f}")

# Градієнтне підсилення з перехресною перевіркою
gb_model = GradientBoostingClassifier(random_state=42)
gb_cv_scores = cross_val_score(gb_model, X_train_scaled, y_train, cv=5, scoring='accuracy')
print(f"Градієнтне підсилення: Середня точність = {gb_cv_scores.mean():.4f}, Стандартне відхилення = {gb_cv_scores.std():.4f}")

# Нейронна мережа з перехресною перевіркою
mlp_model = MLPClassifier(hidden_layer_sizes=(100,), max_iter=500, random_state=42)
mlp_cv_scores = cross_val_score(mlp_model, X_train_scaled, y_train, cv=5, scoring='accuracy')
print(f"Нейронна мережа: Середня точність = {mlp_cv_scores.mean():.4f}, Стандартне відхилення = {mlp_cv_scores.std():.4f}")

print(f"Логістична регресія: Середня точність = {logistic_cv_scores.mean():.4f}, Стандартне відхилення = {logistic_cv_scores.std():.4f}")
print(f"Випадковий ліс: Середня точність = {rf_cv_scores.mean():.4f}, Стандартне відхилення = {rf_cv_scores.std():.4f}")
print(f"Градієнтне підсилення: Середня точність = {gb_cv_scores.mean():.4f}, Стандартне відхилення = {gb_cv_scores.std():.4f}")
print(f"Нейронна мережа: Середня точність = {mlp_cv_scores.mean():.4f}, Стандартне відхилення = {mlp_cv_scores.std():.4f}")

# 7. Оцінка моделі
# Оцініть моделі на основі точності, прецизійності, відгуку, F1-балу та інших відповідних метрик.
from sklearn.metrics import accuracy_score, precision_score, recall_score, f1_score, confusion_matrix, classification_report

print("Оцінка для Логістичної регресії:")
print(f"Точність: {accuracy_score(y_test, y_pred_logistic):.4f}")
print(f"Прецизійність: {precision_score(y_test, y_pred_logistic):.4f}")
print(f"Відгук: {recall_score(y_test, y_pred_logistic):.4f}")
print(f"F1-бал: {f1_score(y_test, y_pred_logistic):.4f}")
print("\nЗвіт класифікації:")
print(classification_report(y_test, y_pred_logistic))


print("Оцінка для Випадкового лісу:")
print(f"Точність: {accuracy_score(y_test, y_pred_rf):.4f}")
print(f"Прецизійність: {precision_score(y_test, y_pred_rf):.4f}")
print(f"Відгук: {recall_score(y_test, y_pred_rf):.4f}")
print(f"F1-бал: {f1_score(y_test, y_pred_rf):.4f}")
print("\nЗвіт класифікації:")
print(classification_report(y_test, y_pred_rf))


print("Оцінка для Градієнтного підсилення:")
print(f"Точність: {accuracy_score(y_test, y_pred_gb):.4f}")
print(f"Прецизійність: {precision_score(y_test, y_pred_gb):.4f}")
print(f"Відгук: {recall_score(y_test, y_pred_gb):.4f}")
print(f"F1-бал: {f1_score(y_test, y_pred_gb):.4f}")
print("\nЗвіт класифікації:")
print(classification_report(y_test, y_pred_gb))


print("Оцінка для Нейронної мережі:")
print(f"Точність: {accuracy_score(y_test, y_pred_mlp):.4f}")
print(f"Прецизійність: {precision_score(y_test, y_pred_mlp):.4f}")
print(f"Відгук: {recall_score(y_test, y_pred_mlp):.4f}")
print(f"F1-бал: {f1_score(y_test, y_pred_mlp):.4f}")
print("\nЗвіт класифікації:")
print(classification_report(y_test, y_pred_mlp))

# Використовуйте матриці помилок для розуміння продуктивності вашої моделі у класифікації виживших та невиживших.

# 1. Логістична регресія
from sklearn.linear_model import LogisticRegression
logistic_model = LogisticRegression()

logistic_model.fit(X_train, y_train)

y_pred_logistic = logistic_model.predict(X_test)

from sklearn.metrics import confusion_matrix
import seaborn as sns
import matplotlib.pyplot as plt

cm_logistic = confusion_matrix(y_test, y_pred_logistic)

plt.figure(figsize=(6, 4))
sns.heatmap(cm_logistic, annot=True, fmt="d", cmap="Blues", xticklabels=['Не вижив', 'Вижив'], yticklabels=['Не вижив', 'Вижив'])
plt.xlabel('Передбачений клас')
plt.ylabel('Реальний клас')
plt.title('Матриця помилок для Логістичної регресії')
plt.show()


# Документація
# 1. Вступ
# Мета проекту - прогнозування ймовірності виживання пасажирів на Титаніку, використовуючи різноманітні методи машинного навчання.

# 2. Збір даних
# Використовували дані з набору даних Титаніка, доступного через бібліотеку Seaborn.

# 3. Очищення та підготовка даних
# Обробка відсутніх значень:
# Заповнення відсутніх значень для віку середнім значенням.
# Заповнення відсутніх значень для порту відправлення найбільш частим значенням.

# Вибір колонок:
#
# Вибрали важливі характеристики: survived, pclass, sex, age, sibsp, parch, fare, embarked.
# Перетворення категорійних змінних:
#
# Використали одноразове кодування для sex та embarked.

# Видалення непотрібних колонок:
# Видалили колонки, які не впливають на модель, наприклад, ticket та name.

# 4. Розвідувальний аналіз даних (EDA)
# Візуалізація ключових характеристик:
# Розподіл виживання.
# Вік пасажирів за виживанням.
# Виживання за класом пасажира.
# Тарифи за виживання.

# Кореляційний аналіз:
# Вивчення кореляцій між змінними за допомогою кореляційної матриці.

# 5. Інженерія ознак
# Створено нові ознаки:
#
# FamilySize = SibSp + Parch + 1
# IsAlone, бінарний індикатор того, чи є пасажир на самоті.
# Масштабування ознак:
#
# Використання StandardScaler для моделей, чутливих до масштабу.

# 6. Вибір моделі
# Розділення даних на навчальний і тестовий набори:

# 7. Оцінка моделі
# Використано метрики: точність, прецизійність, відгук, F1-бал, матриця помилок.

# 8. Висновки
# На основі оцінки продуктивності моделей було встановлено, яка з моделей найкраще прогнозує виживання.
# Результати також підкреслюють значення таких факторів, як клас пасажира, вік та тариф, у прогнозуванні виживання.




