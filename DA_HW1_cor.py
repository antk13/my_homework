from flask import Flask, request
from flask_restful import Resource, Api

app = Flask(__name__)
api = Api(app)

questions = [
    {"id": 1, "question": "What is the capital of France?", "options": "a) Kyiv\nb) London\nc) Paris", "correct_answer": "c"},
    {"id": 2, "question": "What is 2 + 2?", "options": "a) 3\nb) 4\nc) 5", "correct_answer": "b"},
    {"id": 3, "question": "What is the color of the sky?", "options": "a) Green\nb) Red\nc) Blue", "correct_answer": "c"}
]

class CheckAnswer(Resource):
    def get(self):
        id = request.args.get("id")
        answer = request.args.get("answer")
        question = next((q for q in questions if str(q["id"]) == id), None)
        if question:
            return {"result": answer == question["correct_answer"]}
        return {"error": "Question not found"}

class Question(Resource):
    def get(self):
        id = request.args.get("id")
        question = next((q for q in questions if str(q["id"]) == id), None)
        if question:
            return {"id": question["id"], "question": question["question"], "options": question["options"]}
        return {"error": "Question not found"}

api.add_resource(CheckAnswer, '/check_answer')
api.add_resource(Question, '/question')

if __name__ == '__main__':
    app.run(debug=True)
