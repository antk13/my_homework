import pandas as pd

url = "https://archive.ics.uci.edu/ml/machine-learning-databases/iris/iris.data"
column_names = ["sepal_length", "sepal_width", "petal_length", "petal_width", "class"]

iris_df = pd.read_csv(url, header=None, names=column_names)

# 1. Аналіз даних Iris: Завантажте дані Iris у DataFrame та проведіть аналіз. Спробуйте відповісти на такі питання:
# Яка середня довжина чашелистика (sepal length) для кожного виду ірису?
average_sepal_length = iris_df.groupby("class")["sepal_length"].mean()
print(average_sepal_length)

# Яка максимальна ширина листка (petal width) для виду "setosa"?
max_petal_width_setosa = iris_df[iris_df['class'] == 'Iris-setosa']['petal_width'].max()
print(f"Максимальна ширина листка для Iris-setosa: {max_petal_width_setosa} см")


# Яка розподіленість довжини листка (petal length) для всіх ірисів?
petal_length_distribution = iris_df['petal_length'].describe()
print(petal_length_distribution)

import matplotlib.pyplot as plt

plt.hist(iris_df['petal_length'], bins=15, edgecolor='black')
plt.title('Розподіл довжини листка (petal length)')
plt.xlabel('Довжина листка (см)')
plt.ylabel('Частота')
plt.show()


# 2. Фільтрація та відбір даних:
# Використовуючи pandas, виберіть лише ті дані Iris, які відповідають певним умовам:

# Створіть новий DataFrame, в якому будуть лише дані для ірисів виду "versicolor".
versicolor_df = iris_df[iris_df["class"] == "Iris-versicolor"]
print(versicolor_df.head())

# Відфільтруйте дані для ірисів з довжиною листка (petal length) більше 5.0.
long_petal_df = iris_df[iris_df["petal_length"] > 5.0]
print(long_petal_df.head())


# 3. Групування та агрегація:
# Використовуючи групування та агрегацію даних, відповідь на такі питання:

# Яка середня ширина листка (petal width) для кожного виду ірису?
average_petal_width = iris_df.groupby("class")["petal_width"].mean()
print(f"Mean petal width:")
print(average_petal_width)

# Яка мінімальна довжина чашелистика (sepal length) для кожного виду ірису?
print(f"Min sepal length:")
min_sepal_length = iris_df.groupby("class")["sepal_length"].min()
print(min_sepal_length)

# Скільки ірисів кожного виду мають довжину листка (petal length)
# більше за середню довжину листка всіх ірисів?

mean_petal_length = iris_df["petal_length"].mean()
iris_above_mean_petal_length = iris_df[iris_df["petal_length"] > mean_petal_length]
count_above_mean = iris_above_mean_petal_length.groupby("class").size()
print(count_above_mean)
