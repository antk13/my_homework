import timeit


def test_method(n):
    primes = [True] * (n + 1)
    p = 2
    while (p * p <= n):
        if primes[p]:
            for i in range(p * p, n + 1, p):
                primes[i] = False
        p += 1

    prime_numbers = [p for p in range(2, n + 1) if primes[p]]
    return prime_numbers


def test_prime_sieve(data):
    for n in data:
        start_time = timeit.default_timer()
        prime_numbers = test_method(n)
        elapsed_time = timeit.default_timer() - start_time
        print(prime_numbers)
        print(f"list range = {n}, seconds_spent = {elapsed_time:.6f}")
        print()


data_to_test = [100, 1_000, 10_000]

test_prime_sieve(data_to_test)
