#1. Створення масиву та операції: Створіть два одновимірних масиви розміром 5x5 кожен, заповніть їх випадковими числами.
# Потім виконайте наступні операції та виведіть результат:

# Сума елементів кожного масиву.
# Різниця між елементами двох масивів.
# Перемноження елементів двох масивів.
# Підняття кожного елемента першого масиву до ступеня відповідного елемента другого масиву.

import numpy as np

array1 = np.random.randint(1, 10, size=5)
array2 = np.random.randint(1, 10, size=5)

print("Array 1:\n", array1)
print("Array 2:\n", array2)

sum_array1 = np.sum(array1)
sum_array2 = np.sum(array2)
print("\nSum array 1:", sum_array1)
print("Sum array 2:", sum_array2)

difference = array2 - array1
print("\nDifference:", difference)

multiply = array1 * array2
print("\nMultiply:", multiply)

power = np.power(array1, array2)
print("\nPower:", power)


# 2. Індексація та вибірка даних: Створіть двовимірний масив 8x8, який відображає шахову дошку (1 - біла клітина, 0 - чорна клітина).
# Використовуйте індексацію для виведення на екран:
#
# Рядок, представляючи 3-й рядок з дошки.
# Стовпець, представляючи 5-й стовпець з дошки.
# Підмасив, представляючи частину дошки розміром 3x3 в лівому верхньому куті.


chessboard = np.zeros((8, 8))

chessboard[::2, 1::2] = 1
chessboard[1::2, ::2] = 1

print("Chessboard:\n", chessboard)

third_row = chessboard[2]
print("3rd row:\n", third_row)

fifth_column = chessboard[:, 4]
print("5th column:\n", fifth_column)

top_left_3x3 = chessboard[:3, :3]
print("Top left 3x3:\n", top_left_3x3)

# 3. Статистика та робота зі зображеннями:
# Завантажте зображення (наприклад, за допомогою бібліотеки Pillow).
# Перетворіть його в тривимірний NumPy-масив та виведіть інформацію про розмір та тип даних. Проведіть статистичний аналіз:
#
# Знайдіть середнє значення, мінімум та максимум для кожного каналу зображення (R, G, B).
# Підрахуйте загальну суму інтенсивності пікселів та виведіть її.
# Виконайте нормалізацію зображення, розділивши значення кожного пікселя на максимальне значення.

from PIL import Image

image = Image.open(r"C:\Users\HP\PycharmProjects\dan_it_lessons\light.jpg")

image_array = np.array(image)

print(f"Shape: {image_array.shape}")
print(f"Type: {image_array.dtype}")

mean_per_channel = np.mean(image_array, axis=(0, 1))
min_per_channel = np.min(image_array, axis=(0, 1))
max_per_channel = np.max(image_array, axis=(0, 1))

print(f"Mean: {mean_per_channel}")
print(f"Min: {min_per_channel}")
print(f"Max: {max_per_channel}")

total_intensity = np.sum(image_array)
print(f"Total intensity: {total_intensity}")

normalized_image_array = image_array / max_per_channel
print(f"Normalized image array:\n{normalized_image_array}")