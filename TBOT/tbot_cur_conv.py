import os

import telebot

import requests

import json

from dotenv import find_dotenv
from dotenv import load_dotenv


env_file = find_dotenv("bot_token")
load_dotenv(env_file)
TBOT_TOKEN = os.environ.get("TBOT_TOKEN")
bot = telebot.TeleBot(TBOT_TOKEN)

API_URL = "https://api.monobank.ua/bank/currency"
result = requests.get(API_URL)

@bot.message_handler(commands=['start'])
def handle_command(message):
    bot.reply_to(message, 'Вітаю)\nЦей бот вміє конвертувати валюту.')
    bot.send_message(message.chat.id, "Натисніть /insert")

@bot.message_handler(commands=['insert'])
def handle_insert_command(message):
    bot.send_message(message.chat.id, "Будь ласка, введіть суму в грн:")

@bot.message_handler(func=lambda message: message.text.isdigit())
def handle_amount_input(message):
    amount = float(message.text)
    user_data[message.chat.id] = {'amount': amount}
    keyboard = telebot.types.InlineKeyboardMarkup()
    button1 = telebot.types.InlineKeyboardButton('USD', callback_data='usd')
    button2 = telebot.types.InlineKeyboardButton('EUR', callback_data='eur')
    keyboard.add(button1, button2)
    bot.send_message(message.chat.id, 'Оберіть валюту для конвертації:', reply_markup=keyboard)


@bot.callback_query_handler(func=lambda call: call.data in ['usd', 'eur'])
def handle_currency_selection(call):
    amount = user_data.get(call.message.chat.id, {}).get('amount')
    if amount is None:
        bot.send_message(call.message.chat.id, "Будь ласка, введіть суму в грн за допомогою /insert")
        return

    if call.data == 'usd':
        currency_usd = result.json()[0]["rateBuy"]
        converted_amount = amount / currency_usd
        currency = 'USD'
    elif call.data == 'eur':
        currency_eur = result.json()[1]["rateBuy"]
        converted_amount = amount / currency_eur
        currency = 'EUR'

    history = load_history()
    add_to_history(history, call.message.chat.id, amount, converted_amount, currency)

    bot.send_message(call.message.chat.id, f"{amount} грн = {converted_amount:.2f} {currency}")

HISTORY_FILE = "conversion_history.json"

user_data = {}


def load_history():
    try:
        with open(HISTORY_FILE, 'r') as file:
            return json.load(file)
    except FileNotFoundError:
        return []

def save_history(history):
    with open(HISTORY_FILE, 'w') as file:
        json.dump(history, file, ensure_ascii=False, indent=4)

def add_to_history(history, chat_id, amount, converted_amount, currency):
    history.append({
        "chat_id": chat_id,
        "amount": amount,
        "converted_amount": converted_amount,
        "currency": currency
    })
    if len(history) > 10:
        history.pop(0)
    save_history(history)


bot.polling()

